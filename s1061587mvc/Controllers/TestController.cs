﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1061587mvc.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["A"] = 1;
            ViewData["B"] = 1;

            ViewBag.A = 1;
            ViewBag.B = 1;

            return View();
        }
        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }
    }
}