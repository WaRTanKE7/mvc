﻿using s1061587mvc.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1061587mvc.Controllers
{
    public class BmiController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        // GET: Bmi
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {
                var h = data.Height;
                var w = data.Weight;
                h = h / 100;
                var bmi = (w / (h * h));
                if (bmi < 18.5)
                {
                    data.level = "體重過輕";
                }
                else if (bmi >= 18.5 && bmi < 24)
                {
                    data.level = "正常範圍";
                }
                else if (bmi >= 24 && bmi < 27)
                {
                    data.level = "過重";
                }
                else if (bmi >= 27 && bmi < 30)
                {
                    data.level = "輕度肥胖";
                }
                else if (bmi >= 30 && bmi < 35)
                {
                    data.level = "中度肥胖";
                }
                else if (bmi >= 35)
                {
                    data.level = "重度肥胖";
                }
                data.bmi = bmi;
            }
            return View(data);
        }
    }
}